using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    private Animator leverAnim;
    public Animator bridgeAnim;
    private bool interacted;

    private void Start()
    {
        leverAnim = this.GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Projectile" || other.gameObject.tag == "PlayerAttack")
        {
            if (!interacted)
            {
                leverAnim.Play("Interacted");
                bridgeAnim.Play("Interacted");
            }

        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float spd = 4.5f;
    public GameObject hitVFX;
    
    
    void Update()
    {
        transform.position += transform.right* Time.deltaTime* spd;
    }

    private void OnTriggerEnter2D(Collider2D hitbox)
    {
        if(hitbox.tag == "ProjectileHitBox" )
        {
            Instantiate(hitVFX, gameObject.transform.position, Quaternion.identity);
            Destroy(this.gameObject);
        }
    }
}

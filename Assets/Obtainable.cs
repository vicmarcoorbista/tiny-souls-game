using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MyEnum
{
    health,
    mana,
    hybrid
};

public class Obtainable : MonoBehaviour
{
    public MyEnum obtainableType = new MyEnum();

    public int healhAmt;
    public int manaAmt;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            PlayerStats PS = other.gameObject.GetComponent<PlayerStats>();
            if (obtainableType == MyEnum.health)
            {
                PS.Heal(healhAmt);
            }
            else if (obtainableType == MyEnum.mana)
            {
                PS.Heal(manaAmt);
            }
            else if (obtainableType == MyEnum.hybrid)
            {
                PS.Heal(healhAmt);
                PS.Heal(manaAmt);
            }

            Destroy(this.gameObject);
        }
    }
}

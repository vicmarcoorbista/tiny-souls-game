using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundChecker : MonoBehaviour
{
    public Character2DController C2DC;

    private void Start()
    {
        C2DC = GetComponentInParent<Character2DController>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Floor")
        {
            C2DC.anim.SetBool("Grounded",true);
            C2DC.anim.SetBool("isJumping", false);
        }
    }
    /*
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Floor")
        {
            C2DC.anim.SetBool("Grounded", false);
            C2DC.anim.SetBool("isJumping", true);
        }
    }
    */

}

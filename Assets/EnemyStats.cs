using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour
{
    public int maxHealth = 100;
    public int currentHealth;
    public HealthBar healthBar;
    public int maxMana = 20;
    public int currentMana;
    public Transform hpPos;
    private Animator enemyUI;
    private Animator enemyAnim;


    void Start()
    {
        currentHealth = maxHealth;
        currentMana = maxMana;
        healthBar.SetMaxHealth(maxHealth);
        healthBar.SetMaxMana(maxMana);
        enemyUI = healthBar.gameObject.GetComponentInParent<Animator>();
        enemyAnim = gameObject.GetComponent<Animator>();
    }

    public void TakeDmg(int dmg)
    {
        currentHealth -= dmg;
        healthBar.SetHealth(currentHealth);
        if (currentHealth <= 0)
        {
            enemyUI.Play("Out");
            enemyAnim.Play("Death");

        }
    }
}

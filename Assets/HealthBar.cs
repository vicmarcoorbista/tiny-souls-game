using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider hpSlider;
    public Slider mpSlider;

    public void SetMaxHealth(int health)
    {
        hpSlider.maxValue = health;
        hpSlider.value = health;
    }

    public void SetHealth(int health)
    {
        hpSlider.value = health;
    }

    public void SetMaxMana(int mana)
    {
        mpSlider.maxValue = mana;
        mpSlider.value = mana;
    }

    public void SetMana(int mana)
    {
        mpSlider.value = mana;
    }
}

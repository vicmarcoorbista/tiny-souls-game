using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character2DController : MonoBehaviour
{
    public float MovementSpeed = 1;
    public float JumpForce = 10f;
    public float RollForce = 4f;
    public bool chain2Enalbed;
    public GameObject projectile;
    public Projectile projectilePrefab;
    public Transform spawnPt;
    private float movement;
    public bool canThrowProjectile = true;
    private bool m_FacingRight = true;
    public bool canMove = true;
    public Animator anim;
    public GroundChecker gc;
    public Rigidbody2D _rb;
    
    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
    private void Update()
    {
        if (!anim.GetBool("isRolling") && canMove)
        {
            movement = Input.GetAxis("Horizontal");
            transform.position += new Vector3(movement, 0, 0) * Time.deltaTime * MovementSpeed;
        }
        
        if (movement != 0 && anim.GetBool("Grounded") && !anim.GetBool("isJumping") && canMove)
        {
            anim.SetBool("Grounded",true);
            anim.SetBool("isRunning", true);
            anim.SetBool("isJumping", false);

        }
        else if (movement == 0 && anim.GetBool("Grounded") && !anim.GetBool("isJumping") && canMove)
        {
            anim.SetBool("Grounded", true);
            anim.SetBool("isRunning", false);
            anim.SetBool("isJumping", false);
        }

        if (movement > 0 && !m_FacingRight)
        {
            FlipX();
        }
        else if (movement < 0 && m_FacingRight)
        {
            FlipX();
        }

        if (Input.GetButtonDown("Jump") && canMove && Mathf.Abs(_rb.velocity.y)< 0.001f && anim.GetBool("Grounded") && !anim.GetBool("isRolling"))
        {
            _rb.AddForce(new Vector2(0, JumpForce), ForceMode2D.Impulse);
            anim.SetBool("Grounded", false);
            anim.SetBool("isRunning", false);
            anim.SetBool("isJumping", true);
        }

        if (Input.GetButtonDown("Roll") && canMove && Mathf.Abs(_rb.velocity.y) < 0.001f && anim.GetBool("Grounded") && !anim.GetBool("isRolling"))
        {
            float RollForceDir = m_FacingRight ? RollForce : -(RollForce);
            _rb.AddForce(new Vector2(RollForceDir, 0), ForceMode2D.Impulse);
            anim.Play("RollyPolly");
            anim.SetBool("isRunning", false);
            anim.SetBool("isRolling", true);
        }

        if (Input.GetButtonDown("Attack") && anim.GetBool("Grounded") && !anim.GetBool("isRolling") && canMove)
        {
            if (!chain2Enalbed)
            {
                anim.Play("Swing");
            }
            else
            {
                anim.Play("Swing2nd");
            }
        } else if (Input.GetButtonDown("FireProjectile") && anim.GetBool("Grounded") && !anim.GetBool("isRolling") && canMove)
        {
            StartCoroutine(ProjectileThrow());
        }
    }

    IEnumerator ProjectileThrow()
    {
        if (canThrowProjectile)
        {
            anim.Play("ThrowProjectile");
            canThrowProjectile = false;
            yield return new WaitForSeconds(2f);
            canThrowProjectile = true;
        }
        
    }

    public void fireProjectile(GameObject projectilePrefab)
    {
        Instantiate(projectilePrefab, spawnPt.transform.position, transform.rotation);
    }

    public void Chain1Enabled(int status)
    {
        if (status == 0)
        {
            chain2Enalbed = false;
        }
        else 
        {
            chain2Enalbed = true;
        }
    }

    public void RollDeceleration()
    {
        float RollForceDir = m_FacingRight ? -(RollForce) : RollForce;
        _rb.AddForce(new Vector2(RollForceDir / 4, 0), ForceMode2D.Impulse);
    }

    public void RollEnd()
    {
        anim.SetBool("isRolling", false);
    }

    private void FlipX()
    {
        m_FacingRight = !m_FacingRight;
        transform.Rotate(0f, 180f, 0f);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TriggerMechanic : MonoBehaviour
{
    public Animator enemyUI;
    public EnemyStats enemy;

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.tag == "Player" && enemy.currentHealth > 0)
        {
            enemyUI.Play("In");
        }
    }
}

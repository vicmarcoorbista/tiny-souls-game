using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public int maxHealth = 100;
    public int currentHealth;
    public HealthBar healthBar;
    public int atkDmg = 10;
    public int maxMana = 20;
    public int currentMana;
    public bool canTakeDMG = true;
    private Character2DController C2DC;
    

    void Start()
    {
        currentHealth = maxHealth;
        currentMana = maxMana;
        healthBar.SetMaxHealth(maxHealth);
        healthBar.SetMaxMana(maxMana);
        C2DC = this.gameObject.GetComponent<Character2DController>();
    }

    public void TakeDmg(int dmg)
    {
        if (!C2DC.anim.GetBool("isRolling") && canTakeDMG)
        {
            StartCoroutine(DamageProcess(dmg));
        }
    }

    IEnumerator DamageProcess(int dmg)
    {
        currentHealth -= dmg;
        healthBar.SetHealth(currentHealth);
        canTakeDMG = false;
        if (currentHealth <= 0)
        {
            C2DC.anim.Play("Death");
            C2DC.canMove = false;
            C2DC._rb.gravityScale = 0.0f;
        }
        yield return new WaitForSeconds(1f);
        canTakeDMG = true;
    }

    public void Heal(int hp)
    {
        if (currentHealth > 0)
        {
            currentHealth += hp;
        }
    }

    public void Mana(int mana)
    {
        if (currentHealth > 0)
        {
            currentMana += mana;
        }
    }

}

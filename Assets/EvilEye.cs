using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EvilEye : MonoBehaviour
{
    public AttackSensor rightSensor;
    public AttackSensor leftSensor;
    public bool isAwake;
    public bool proneEye;
    public GameObject bridge;
    private bool isAttacking;
    private Animator anim;

    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
    }


    void Update()
    {
        if (rightSensor.detectedPlayer != null && leftSensor.detectedPlayer == null && !isAwake)
        {
            StartCoroutine(RightAwake());
            isAwake = true;
        }
        else if (rightSensor.detectedPlayer == null && leftSensor.detectedPlayer != null && !isAwake)
        {
            StartCoroutine(LeftAwake());
            isAwake = true;
        }

        if (rightSensor.detectedPlayer != null && leftSensor.detectedPlayer == null && isAwake && !isAttacking)
        {
            anim.Play("SwordSwingRight");
            isAttacking = true;
            Invoke("notAttacking", 2f);
        }
        else if (rightSensor.detectedPlayer == null && leftSensor.detectedPlayer != null && isAwake && !isAttacking)
        {
            anim.Play("SwordSwing");
            isAttacking = true;
            Invoke("notAttacking", 2f);
        }

        if (rightSensor.detectedPlayer == null && leftSensor.detectedPlayer == null && isAwake && !isAttacking)
        {
            anim.Play("Idle");
            isAwake = false;
        }
    }

    IEnumerator RightAwake()
    {
        anim.Play("AwakeRight");
        yield return new WaitForSeconds(1f);
        if (rightSensor.detectedPlayer != null && leftSensor.detectedPlayer == null && !isAttacking)
        {
            anim.Play("SwordSwingRight");
            isAttacking = true;
            Invoke("notAttacking", 2f);
        }
    }

    IEnumerator LeftAwake()
    {
        anim.Play("AwakeLeft");
        yield return new WaitForSeconds(1f);
        if (rightSensor.detectedPlayer == null && leftSensor.detectedPlayer != null && !isAttacking)
        {
            anim.Play("SwordSwing");
            isAttacking = true;
            Invoke("notAttacking",2f);
        }
    }

    public void notAttacking()
    {
        isAttacking = false;
    }

    public void ProneEye()
    {
        proneEye = true;
    }

    public void NotProneEye()
    {
        proneEye = false;
    }

    public void activateBridge()
    {
        bridge.SetActive(true);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamageReceiver : MonoBehaviour
{
    public EnemyStats myStats;
    public PlayerStats playerStats;
    public EvilEye evilEye;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "PlayerAttack")
        {
            if (evilEye.proneEye)
            {
                PlayerStats playerstats = other.gameObject.GetComponentInParent<PlayerStats>();
                myStats.TakeDmg(playerstats.atkDmg);
            }
        }
    }
}

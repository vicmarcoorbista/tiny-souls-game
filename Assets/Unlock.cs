using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unlock : MonoBehaviour
{
    public int status = 0;
    public int index;
    public Transform spawnPt;
   
    void Awake()
    {
        if (status == 1)
        {
            this.GetComponent<Animator>().SetBool("Unlocked", true);
        }
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" && status == 0)
        {
            this.GetComponent<Animator>().SetBool("Unlocked", true);
            status = 1;
        }
    }

}

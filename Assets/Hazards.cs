using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hazards : MonoBehaviour
{
    public int dmg;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            PlayerStats stats = other.gameObject.GetComponent<PlayerStats>();

            if (stats.currentHealth > 0)
            {
                stats.TakeDmg(dmg);
            }

        }
    }
}
